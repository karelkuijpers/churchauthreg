<?php
defined('TYPO3') or die();

/***************
 * Plugin
 */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Churchauthreg',
    'Permission',
    'Church website users administration'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['churchauthreg_permission'] = 'recursive';
