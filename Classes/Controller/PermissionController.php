<?php
namespace Parousia\Churchauthreg\Controller;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Extbase\Property\PropertyMapper;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Http\ForwardResponse;

use Parousia\Churchauthreg\Domain\Model\Search;
use Parousia\Churchauthreg\Domain\Model\Permission;
use Parousia\Churchauthreg\Domain\Repository\PermissionRepository;
use Parousia\Churchlogin\Domain\Repository\LoginRepository;



/***
 *
 * This file is part of the "churcauthreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * PermissionController
 */
class PermissionController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    protected $LoginRepository;
    protected $PermissionRepository; 
	protected $PropertyMapper;
	var $args=array();
	var $stack='';
	var $pid=0;
	var $downloadfile='';
	var $search='';
	var $usergroup='';
	var $pageuidperson='';
	var $defaultusergroup='';
	var $permissions=array();
	var $pageId='';
	var $userid='';
	var $frontendUser;

	
	public function __construct(
       	PermissionRepository $permissionRepository,
       	LoginRepository $loginRepository,
		PropertyMapper $propertyMapper,
    ) 
	{	
		$this->PermissionRepository = $permissionRepository;
		$this->LoginRepository = $loginRepository;
		$this->PropertyMapper = $propertyMapper;
	}

    /**
     * Initialize 
     */
    public function initializeAction(): void
    {
		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		if (isset($this->args['downloadfile']))
		{
			$this->downloadfile=$this->args['downloadfile'];
		}
		if (count($this->args)<2 and is_array($this->request->getParsedBody()))
		{
			$ParsedBody=$this->request->getParsedBody();
			if (isset($ParsedBody['tx_churchauthreg_permission']['search']))
				$this->args['search']=$ParsedBody['tx_churchauthreg_permission']['search'];
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Initialize args from ParsedBody '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			if (isset($ParsedBody['tx_churchauthreg_permission']['usergroup']))
				$this->args['usergroup']=$ParsedBody['tx_churchauthreg_permission']['usergroup'];
			if (isset($ParsedBody['tx_churchauthreg_permission']['permission']))
				$this->args['permission']=$ParsedBody['tx_churchauthreg_permission']['permission'];
		}

		if (isset($this->args['search']))
		{
			$this->search=$this->args['search'];
		} else $this->search=NULL;
		if (isset($this->args['usergroup']))
		{
			$this->usergroup=$this->args['usergroup'];
		} else $this->usergroup=NULL;
	
		if (isset($this->args['stack']))
		{
			$this->stack=$this->args['stack'];
		} else $this->stack=NULL;
		if (!empty($this->stack))
		{
			$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction stack: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		} else $this->stack=array();

		$pageArguments = $this->request->getAttribute('routing');
		$this->pageId = $pageArguments->getPageId();
		$this->frontendUser = $this->request->getAttribute('frontend.user');
		$this->userid=$this->frontendUser->user['person_id'];
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchlogin');	
		$this->pid=$extensionConfiguration['storagePid'];
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchauthreg');
		$this->pageuidperson = $extensionConfiguration['pagenbrsingleperson'];
		$this->defaultusergroup= $extensionConfiguration['defaultusergroup'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction required permission string: '.$extensionConfiguration['requiredpermissions']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 			
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction required permission array: '.urldecode(http_build_query(explode(',',$extensionConfiguration['requiredpermissions']),NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 			
		$this->permissions=explode(',',$extensionConfiguration['requiredpermissions']);
		sort($this->permissions,SORT_NATURAL | SORT_FLAG_CASE);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction required permission sorted: '.urldecode(http_build_query($this->permissions,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 			
	}    
	
    /**
     * Initialize search
     */
    public function initializesearchUserAction(): void
    {
		$this->PermissionRepository->InitializeAuth($this->pid,$this->defaultusergroup,$this->permissions);
	}    
	/**
     * action searcUser
     *
     * @return void
     */
    public function searchUserAction(): ResponseInterface
    {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SearchUser search start'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		$Search= new Search();
		if (!empty($this->stack))
		{
			$key=array_search('selectedList', array_column($this->stack, 'action'));
			if ($key)$Search->SetGeneral($this->stack[$key]["search"]['general']);
		}
		
/*        if (empty($this->search)) 
		{
            $this->search = array();
			$this->search['general']="";
        } */
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SearchForm search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		// reinitialize stack:
		$this->stack=array(array("action"=>"searchUser","search"=>$this->search));
//var_dump($this->search);
    	$assignedValues = [
			'search' => $Search,
			'downloadfile' => $this->downloadfile,
			'stack' => json_encode($this->stack),
			'admin' => (int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC),
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SearchForm assignedValues: '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
		//$this->view->assign('search',$this->search);
    }

    /**
     * action selectedList
     *
     * @return void
     */
    public function selectedListAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search : '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack : '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		if (empty($this->stack))$this->stack=[array('action'=>"searchUser","search"=>$this->search),array("action"=>"selectedList","search"=>$this->search)];
		else
		{
			$key=array_search('selectedList', array_column($this->stack, 'action'));
			if (!$key)$this->stack[]=array("action"=>"selectedList","search"=>$this->search);
			else 
			{
				$this->search=$this->stack[$key]['search'];
				array_splice($this->stack,$key+1);  // reset stack from this action off
			}
		}
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		if (empty($this->search['general']) )
		{
	        return (new ForwardResponse('searchUser'));
		}
		//$results=print_r(get_object_vars($this->PermissionRepository),true);   ????
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'PermissionController selectedListAction repository:'.get_class($this->PermissionRepository).'; vars:'.$results."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		
		$persons=$this->PermissionRepository->findSelection($this->search);
    	$assignedValues = [
			'persons' => $persons,
			'stack' => json_encode($this->stack),
			'downloadfile' => $this->downloadfile,
			'userid' => $this->userid,
			'admin' => (int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC),
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }


    /**
     * action detail
     *
     * @return void
     */
    public function detailUserAction(): ResponseInterface
    { 
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		$personid=0;
		if (!empty($this->args['personid']))$personid=$this->args['personid'];
		//$userid=$this->frontendUser->user['person_id'];
		$key=false;
		if (is_array($this->stack))$key=array_search('detailUser', array_column($this->stack, 'action'));
		if ($key===false )
		{
			// forward:
			if (is_array($this->stack))
			{
				$ptr=count($this->stack)-1;
				$return=$this->stack[$ptr]["action"];
			}
			$this->stack[]=array("action"=>"detailUser","controller"=>"Permission","extensionName"=>"churchauthreg","pluginName"=>"Permission","pageUid"=>$this->pageId,"personid"=>$personid);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailUser forward return: '.$return.'; stack na splice:'.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		}
		else
		{
			if (!empty($this->stack[$key]['personid']))$personid=$this->stack[$key]['personid'];
			array_splice($this->stack,$key+1);  // update: reset stack after this action off
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailUser key: '.$key.'; personid:'.$personid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
			$return=$this->stack[$key-1]["action"];;
		}
	
        $person = $this->PermissionRepository->findByUid($personid);
//		if (isset($person['lastupdpwd']))$lastUpdPwd = new \DateTime($person['lastupdpwd']);else $lastUpdPwd=null;
//		$person['lastupdpwd']=$lastUpdPwd;
		if (isset($person['usergroup']))$person['usergroup']=explode(",",trim($person["usergroup"]));
		if (isset($person['feuseruid']))$feuserid=$person['feuseruid'];else $feuserid='';
		$Permission= $this->PropertyMapper->convert($person,Permission::class); 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailUser person from db: '..urldecode(http_build_query($person,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		$usergroups= $this->PermissionRepository->findUsergroups();
		$resultingpermissions=$this->PermissionRepository->FindFunctionalities($feuserid);
	
		$assignedValues = [
			'personid' => $personid,
			'person' => $Permission,
			'usergroups' =>$usergroups,
			'stack' => json_encode($this->stack),
			'return'=>$return,		
			'pageuidperson' => $this->pageuidperson,	
			'defaultusergroup' => $this->defaultusergroup,	
			'resultingpermissions' => $resultingpermissions,
			'admingroup' => churchpersreg_div::ADMINGROUP,
			'ownuser' => ($this->userid==$personid),
        ];
        $this->view->assignMultiple($assignedValues); 
		$assetCollector = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\AssetCollector::class);
		$assetCollector->addJavaScript('multiselect', 'EXT:parousiazoetermeer/Resources/Public/JavaScript/multiselect.js',[], ['priority' => true]);
		return $this->htmlResponse();
    } 

	
	    /**
     * action selectedListUsergroup
     *
     * @return void
     */
    public function selectedListUsergroupAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListUser search : '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack : '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		$key=array_search('selectedListUsergroup', array_column($this->stack, 'action'));
		if (!$key)
		{
			$key=count($this->stack);
			$return=$this->stack[$key-1]["action"];;
			$this->stack[]=array("action"=>"selectedListUsergroup");
		}
		else 
		{
			array_splice($this->stack,$key+1);  // reset stack from this action off
			$return=$this->stack[$key-1]["action"];;
		}
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'PermissionController selectedListAction repository:'.get_class($this->PermissionRepository).'; vars:'.$results."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		$usergroups= $this->PermissionRepository->FindAllUsergroups($this->defaultusergroup);
    	$assignedValues = [
			'usergroups' => $usergroups,
			'stack' => json_encode($this->stack),
			'downloadfile' => $this->downloadfile,
			'return' => $return,
			'usergroupid' => '',
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }

	    /**
     * action detailUsergroup
     *
     * @return void
     */
    public function detailUsergroupAction(): ResponseInterface
    { 
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		if(!empty($this->args['fegroupid']))$usergroupid=$this->args['fegroupid'];else $usergroupid=null;
		$key=array_search('detailUsergroup', array_column($this->stack, 'action'));
		if ($key===false )
		{
			// forward:
			$ptr=count($this->stack)-1;
			$return=$this->stack[$ptr]["action"];
			$this->stack[]=array("action"=>"detailUsergroup","controller"=>"Permission","extensionName"=>"churchauthreg","pluginName"=>"Permission","pageUid"=>$this->pageId,"usergroupid"=>$usergroupid);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailUsergroup forward return: '.$return.'; stack na splice:'.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		}
		else
		{
			//$usergroupid=$this->stack[$key]['usergroupid'];
			array_splice($this->stack,$key+1);  // update: reset stack after this action off
//			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailUsergroup key: '.$key.'; personid:'.$personid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
			$return=$this->stack[$key-1]["action"];;
		}
	
        $usergroup = $this->PermissionRepository->findUsergroupByUid($usergroupid);
		if(!isset($usergroup))$usergroup['description']='';
		if(isset($usergroup["function_uids"]))$usergroup['function_uids']=explode(",",trim($usergroup["function_uids"]));else $usergroup["function_uids"]='';
		$functionalities= $this->PermissionRepository->FindFunctionalities();
		$functionalityoption=array();
		foreach ($functionalities as $functionality) $functionalityoption[$functionality['f_uid']]=$functionality['naam'];

	
		$assignedValues = [
			'usergroupid' => $usergroupid,
			'functionalityoption' => $functionalityoption,
			'usergroup' =>$usergroup,
			'stack' => json_encode($this->stack),
			'return'=>$return,
			'admingroup' => churchpersreg_div::ADMINGROUP,
			'defaultusergroup' => $this->defaultusergroup,
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailUsergroup assignedValues: '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
        $this->view->assignMultiple($assignedValues); 
		$assetCollector = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\AssetCollector::class);
		$assetCollector->addJavaScript('multiselect', 'EXT:parousiazoetermeer/Resources/Public/JavaScript/multiselect.js',[], ['priority' => true]);
		return $this->htmlResponse();
    } 

	    /**
     * action selectedListUsergroup
     *
     * @return void
     */
    public function selectedListFunctionAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListUser search : '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack : '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		$key=array_search('selectedListFunction', array_column($this->stack, 'action'));
		if (!$key)
		{
			$key=count($this->stack);
			$return=$this->stack[$key-1]["action"];;
			$this->stack[]=array("action"=>"selectedListFunction");
		}
		else 
		{
			array_splice($this->stack,$key+1);  // reset stack from this action off
			$return=$this->stack[$key-1]["action"];;
		}
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'PermissionController selectedListAction repository:'.get_class($this->PermissionRepository).'; vars:'.$results."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		$functionalities= $this->PermissionRepository->FindAllFunctionalities();
//		if (empty($functionalities) or 	($key=array_search(churchpersreg_div::ADMINFUNC, array_column($functionalities, 'naam'))===false)
		

    	$assignedValues = [
			'functionalities' => $functionalities,
			'stack' => json_encode($this->stack),
			'downloadfile' => $this->downloadfile,
			'return' => $return,
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }

	    /**
     * action detailFunctionality
     *
     * @return void
     */
    public function detailFunctionalityAction(): ResponseInterface
    { 
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		if(!empty($this->args['f_uid']))$functionalityid=$this->args['f_uid'];else $functionalityid=0;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailFunctionality args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		$key=array_search('detailFunctionality', array_column($this->stack, 'action'));
		if ($key===false )
		{
			// forward:
			$ptr=count($this->stack)-1;
			$return=$this->stack[$ptr]["action"];
			$this->stack[]=array("action"=>"detailFunctionality","controller"=>"Permission","extensionName"=>"churchauthreg","pluginName"=>"Permission","pageUid"=>$this->pageId,"f_uid"=>$functionalityid);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailFunctionality forward return: '.$return.'; stack na splice:'.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		}
		else
		{
			//$functionalityid=$this->stack[$key]['f_uid'];
			array_splice($this->stack,$key+1);  // update: reset stack after this action off
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailFunctionality key: '.$key.'; functionalityid:'.$functionalityid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
			$return=$this->stack[$key-1]["action"];;
		}
	
        $functionality = $this->PermissionRepository->findFunctionaltyByUid($functionalityid);
		$functionality['securitylevelold']=$functionality['securitylevel'];
	
		$assignedValues = [
			'f_uid' => $functionalityid,
			'functionality' =>$functionality,
			'stack' => json_encode($this->stack),
			'return'=>$return,		
			'adminfunc'=> ($functionality['naam']==churchpersreg_div::ADMINFUNC),
			'requiredpermission' => (in_array($functionality['naam'],$this->permissions)||($functionality['naam']==churchpersreg_div::ADMINFUNC)),
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailFunctionality assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
       $this->view->assignMultiple($assignedValues); 
	   return $this->htmlResponse();
    } 

    /**
     * action overview
     *
     * @return void
     */
    public function saveUsergroupAction(): ResponseInterface
    { 
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		if(!empty($this->args['usergroup']))$usergroup=$this->args['usergroup'];else $usergroup=array();
		$usergroup['pid']=$this->pid;
		$usergroupid=$usergroup['uid'];
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SAVE user args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		$key=array_search('detailUsergroup', array_column($this->stack, 'action'));
		if ($key)
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user permission: '.str_replace('=','="',urldecode(http_build_query($usergroup,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
	        $ErrMsg=$this->PermissionRepository->saveUsergroup($usergroup); 
			if (empty($ErrMsg))	
			{
				if (empty($usergroupid))
					array_splice($this->stack,$key);  // new: reset stack from this action off
				else
				{ 
					array_splice($this->stack,$key+1);  // update: reset stack after this action off
					$this->LoginRepository->securityupdate('','');
				}
			}
		}
		$assignedValues = [
			'stack' => json_encode($this->stack),
		];
		//if (empty($usergroupid)) $assignedValues['usergroupid']=$usergroup['uid']; 
		$assignedValues['usergroupid']='';
        return (new ForwardResponse('selectedListUsergroup'))
               ->withArguments($assignedValues);
		
	}

    /**
     * action saveFunctionality
     *
     * @return void
     */
    public function saveFunctionalityAction(): ResponseInterface
    { 
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		if(!empty($this->args['functionality']))$functionality=$this->args['functionality'];else $functionality=array();
		if(isset($functionality['f_uid']))$functionalityid=$functionality['f_uid'];else $functionalityid='';
	//	//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveFunctionalityAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		$key=array_search('detailFunctionality', array_column($this->stack, 'action'));
		if ($key)
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user permission: '.str_replace('=','="',urldecode(http_build_query($functionality,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
	        $ErrMsg=$this->PermissionRepository->saveFunctionality($functionality);
 
			if (empty($ErrMsg))	
			{
				if (empty($functionalityid))
					array_splice($this->stack,$key);  // new: reset stack from this action off
				else
				{ 
					array_splice($this->stack,$key+1);  // update: reset stack after this action off
					if ($functionality['securitylevelold']!=$functionality['securitylevel'])
					{
						// update securitylevel of correponding frontend users:
						$this->LoginRepository->securityupdate('','');
					}
				}
			}
		}
		$assignedValues = [
			'stack' => json_encode($this->stack),
		];
		$assignedValues['functionalityid']=''; 
       	return (new ForwardResponse('selectedListFunction'))
              ->withArguments($assignedValues);
		
	}

    /**
     * action overview
     *
     * @return void
     */
    public function overviewAction(): ResponseInterface
    { 
		if (!(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))die("You don't have the privilege to perform this action");
		$allpermissions=$this->PermissionRepository->FindOverviewFunctionalities($this->defaultusergroup);
		$styles=array(['valign'=>'top'],['valign'=>'top','wrap_text'=>true]);
		$col_options = array('widths'=>[30,30]);
		$exportfilenaam=churchpersreg_div::ExportToXlsx($allpermissions,"exportpermissions",'','',$this->frontendUser->user['name'],$styles,$col_options);
		$this->stack=array();
		$assignedValues = [
			'downloadfile' => "https://".$_SERVER['HTTP_HOST']."/".$exportfilenaam,
			'stack' => json_encode($this->stack),
			'return' => 'searchUser',
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Export action:'.$action.'; assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
       	return (new ForwardResponse('searchUser'))
              ->withArguments($assignedValues);
    } 

	 /**
     * action export
     *
     * @return void
     */
    public function exportAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("Export personen"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'EXPORT persons args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$ErrMsg='';
		$key=array_search('selectedList', array_column($this->stack, 'action'));
		if ($key)
		{
			$action='selectedList';
			$this->search=$this->stack[$key]['search'];
			array_splice($this->stack,$key+1);  // reset stack from this action off
			$persons=$this->PermissionRepository->findExportPersons($this->search);
			$exportfilenaam=churchpersreg_div::ExportToXlsx($persons,"exportfrontendusers",'','',$this->frontendUser->user['name']);
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (empty($action)) $action='searchUser';

		$assignedValues = [
			'downloadfile' => "https://".$_SERVER['HTTP_HOST']."/".$exportfilenaam,
			'stack' => json_encode($this->stack),
			'return' => $action,
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Export action:'.$action.'; assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
       	return (new ForwardResponse($action))
              ->withArguments($assignedValues);
	}

    /**
     * action saveUser
     *
     * @return void
     */
    public function saveUserAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		if(!empty($this->args['permission']))$Permission=$this->args['permission'];else $Permission=array();
		$Permission['pid']=$this->pid;
		if(!empty($Permission['personuid']))$personid=$Permission['personuid'];else $personid='';
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SAVE user args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		$key=array_search('detailUser', array_column($this->stack, 'action'));
		if ($key)
		{
	//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user permission: '.str_replace('=','="',urldecode(http_build_query($Permission,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
	        $ErrMsg=$this->PermissionRepository->saveUser($Permission); 
			if (empty($ErrMsg))	
			{
				if (empty($personid))
					array_splice($this->stack,$key);  // new: reset stack from this action off
				else 
					array_splice($this->stack,$key+1);  // update: reset stack after this action off
				// update securitylevel:
				if(isset($Permission['feuseruid']))$feuser_uid=intval($Permission['feuseruid']);else $feuser_uid='';
				$this->LoginRepository->securityupdate($feuser_uid);
			}
		}
		$assignedValues = [
			'stack' => json_encode($this->stack),
		];
		if (empty($personid) and !empty($Permission['personuid'])) $assignedValues['personid']=$Permission['personuid']; 
       	return (new ForwardResponse('selectedList'))
              ->withArguments($assignedValues);
    }


    /**
     * action deleteUsergroup
     *
     * @return void
     */
    public function deleteUsergroupAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		if(isset($this->args['uid']))$usergroupid=$this->args['uid'];else $usergroupid='';
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SAVE user args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		$key=array_search('detailUsergroup', array_column($this->stack, 'action'));
		if ($key)
		{
	//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user permission: '.str_replace('=','="',urldecode(http_build_query($Permission,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
	        $ErrMsg=$this->PermissionRepository->deleteUsergroup($usergroupid); 
			if (empty($ErrMsg))	
			{
				if (empty($usergroupid))
					array_splice($this->stack,$key);  // new: reset stack from this action off
				else 
					array_splice($this->stack,$key+1);  // update: reset stack after this action off
			}
		}
		$assignedValues = [
			'stack' => json_encode($this->stack),
		];
       	return (new ForwardResponse('selectedListUsergroup'))
              ->withArguments($assignedValues);
    }
	
    /**
     * action deleteFunctionality
     *
     * @return void
     */
    public function deleteFunctionalityAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		if(isset($this->args['f_uid']))$functionalityid=$this->args['f_uid'];else $functionalityid='';
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SAVE user args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		$key=array_search('detailFunctionality', array_column($this->stack, 'action'));
		if ($key)
		{
	//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save user permission: '.str_replace('=','="',urldecode(http_build_query($Permission,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
	        $ErrMsg=$this->PermissionRepository->deleteFunctionality($functionalityid); 
			if (empty($ErrMsg))	
			{
				if (empty($functionalityid))
					array_splice($this->stack,$key);  // new: reset stack from this action off
				else 
					array_splice($this->stack,$key+1);  // update: reset stack after this action off
			}
		}
		$assignedValues = [
			'stack' => json_encode($this->stack),
		];
       	return (new ForwardResponse('selectedListFunction'))
              ->withArguments($assignedValues);
    }
	
	
}