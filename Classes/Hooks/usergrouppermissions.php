<?php
namespace Parousia\Churchauthreg\Hooks;

ini_set("display_errors",1);
ini_set("log_errors",1);
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;

class usergrouppermissions 
{
	protected $db;

/**
 * @param ServerRequestInterface $request
 * @param ResponseInterface $response
 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{

		/*
		* file for ajax to update person idperson to participate in cleanteams or not
		 * Created on 2 June 2017
		 * Post parameters: 
		 	idperson,
			cleanteam,
		 */
	    
		//$person_id=69;
		$response = GeneralUtility::makeInstance(Response::class);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin usergrouppermissions: "."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Hooks/debug.log');

		$aParms=$request->getParsedBody(); 
	    //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin usergrouppermissions:".http_build_query($aParms,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Hooks/debug.log');

		if (isset($aParms["usergroup"])) $usergroup=$aParms["usergroup"];
		else die("You are not privileged to perform this action");

		
		churchpersreg_div::connectdb($this->db);
		$query='SELECT concat("<p>",GROUP_CONCAT(DISTINCT f.naam order by f.naam separator "</p><p>"),"</p>") as permissions FROM rm_functionaliteit f, rm_gg_f as gg_f WHERE f.f_uid=gg_f.f_uid and FIND_IN_SET(gg_f.gg_uid,"'.$usergroup.'") order by f.naam';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": usergrouppermissions select functionalities:".$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Hooks/debug.log');
		$result=$this->db->query($query) or die("Can't perform Query");	
		$row=$result->fetch_array(MYSQLI_ASSOC);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": usergrouppermissions return json:".$row['permissions']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Hooks/debug.log');
		$response->getBody()->write($row['permissions']);
		return $response;
	}
	 
}

 

