<?php
namespace Parousia\Churchauthreg\Domain\Model;

/***
 *
 * This file is part of the "Sermons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * A speaker is a permission who does the preaching
 */
class Permission extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * personuid
     *
     * @var int
     */
    protected $personuid = 0;

    /**
     * feuseruid
     *
     * @var int
     */
    protected $feuseruid = 0;

    /**
     * pid'.
     *
     * @var int
     */
    protected $pid = 0;

    /**
     * usergroup
     *
     * @var array
     */
    protected $usergroup = array();

    /**
     * permissionid
     *
     * @var int
     */
    protected $permissionid = 0;

    /**
     * naam
     *
     * @var string
     */
    protected $naam = '';

    /**
     * achternaam
     *
     * @var string
     */
    protected $achternaam = '';

    /**
     * adres
     *
     * @var string
     */
    protected $adres = '';

    /**
     * straatnaam
     *
     * @var string
     */
    protected $straatnaam = '';

    /**
     * huisnummer
     *
     * @var string
     */
    protected $huisnummer = '';
	
    /**
     * postcode
     *
     * @var string
     */
    protected $postcode = '';

    /**
     * woonplaats
     *
     * @var string
     */
    protected $woonplaats = '';

    /**
     * land
     *
     * @var string
     */
    protected $land = '';

    /**
     * username
     *
     * @var string
     */
    protected $username = '';

    /**
     * securitylevel
     *
     * @var string
     */
    protected $securitylevel = '';

    /**
     * lastupdpwd
     *
     * @var /DateTime
     */
    protected $lastupdpwd = NULL;

    /**
     * lstinlog
     *
     * @var string
     */
    protected $lstinlog = 0;

    /**
     * aantalmaalnigelogd
     *
     * @var int
     */
    protected $aantalmaalingelogd = 0;

    /**
     * geblokkeerd
     *
     * @var int
     */
    protected $geblokkeerd = 0;

    /**
     * txgoogleauthenabled
     *
     * @var int
     */
    protected $txgoogleauthenabled = 0;


/*  ********** GETTERS and SETTERS *************
*
*/

    /**
     * Returns the personuid
     *
     * @return int $personuid
     */
    public function getPersonuid(): int
    {
        return $this->personuid;
    }
    /**
     * Sets the personuid
     *
     * @param int $personuid
     * @return void
     */
    public function setPersonuid($personuid):void
    {
        $this->personuid = $personuid;
    }

    /**
     * Returns the feuseruid
     *
     * @return int $feuseruid
     */
    public function getFeuseruid(): int
    {
        return $this->feuseruid;
    }
    /**
     * Sets the feuseruid
     *
     * @param int $feuseruid
     * @return void
     */
    public function setFeuseruid($feuseruid):void
    {
        $this->feuseruid = $feuseruid;
    }

    /**
     * Returns the pid
     *
     * @return int $pid
     */
    public function getPid(): int
    {
        return $this->pid;
    }
    /**
     * Sets the pid
     *
     * @param int $pid
     * @return void
     */
    public function setPid($pid):void
    {
        $this->pid = $pid;
    }

    /**
     * Returns the usergroup
     *
     * @return array|null $usergroup
     */
    public function getUsergroup(): ?array
    {
        return $this->usergroup;
    }
    /**
     * Sets the usergroup
     *
     * @param array $usergroup
     * @return void
     */
    public function setUsergroup($usergroup):void
    {
        $this->usergroup = $usergroup;
    }

    /**
     * Returns the permissionid
     *
     * @return int $permissionid
     */
    public function getPermissionid(): int
    {
        return $this->permissionid;
    }
    /**
     * Sets the permissionid
     *
     * @param int $permissionid
     * @return void
     */
    public function setPermissionid($permissionid):void
    {
        $this->permissionid = $permissionid;
    }
  
    /**
     * Returns the naam
     *
     * @return string $naam
     */
    public function getNaam(): string
    {
        return $this->naam;
    }
    /**
     * Sets the naam
     *
     * @param string $naam
     * @return void
     */
    public function setNaam($naam):void
    {
        $this->naam = $naam;
    }

    /**
     * Returns the achternaam
     *
     * @return string $achternaam
     */
    public function getAchternaam(): string
    {
        return $this->achternaam;
    }
    /**
     * Sets the achternaam
     *
     * @param string $achternaam
     * @return void
     */
    public function setAchternaam($achternaam):void
    {
        $this->achternaam = $achternaam;
    }

    /**
     * Returns the adres
     *
     * @return string $adres
     */
    public function getAdres(): string
    {
        return $this->adres;
    }
    /**
     * Sets the adres
     *
     * @param string $adres
     * @return void
     */
    public function setAdres($adres):void
    {
        $this->adres = $adres;
    }

    /**
     * Returns the straatnaam
     *
     * @return string $straatnaam
     */
    public function getStraatnaam(): string
    {
        return $this->straatnaam;
    }
    /**
     * Sets the straatnaam
     *
     * @param string $straatnaam
     * @return void
     */
    public function setStraatnaam($straatnaam):void
    {
        $this->straatnaam = $straatnaam;
    }

    /**
     * Returns the huisnummer
     *
     * @return string $huisnummer
     */
    public function getHuisnummer(): string
    {
        return $this->huisnummer;
    }
    /**
     * Sets the huisnummer
     *
     * @param string $huisnummer
     * @return void
     */
    public function setHuisnummer($huisnummer):void
    {
        $this->huisnummer = $huisnummer;
    }

    /**
     * Returns the postcode
     *
     * @return string $postcode
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }
    /**
     * Sets the postcode
     *
     * @param string $postcode
     * @return void
     */
    public function setPostcode($postcode):void
    {
        $this->postcode = $postcode;
    }

    /**
     * Returns the woonplaats
     *
     * @return string $woonplaats
     */
    public function getWoonplaats(): string
    {
        return $this->woonplaats;
    }
    /**
     * Sets the woonplaats
     *
     * @param string $woonplaats
     * @return void
     */
    public function setWoonplaats($woonplaats):void
    {
        $this->woonplaats = $woonplaats;
    }

    /**
     * Returns the username
     *
     * @return string $username
     */
    public function getUsername(): string
    {
        return $this->username;
    }
    /**
     * Sets the username
     *
     * @param string $username
     * @return void
     */
    public function setUsername($username):void
    {
        $this->username = $username;
    }

    /**
     * Returns the securitylevel
     *
     * @return string $securitylevel
     */
    public function getSecuritylevel(): string
    {
        return $this->securitylevel;
    }

    /**
     * Sets the securitylevel
     *
     * @param string $securitylevel
     * @return void
     */
    public function setSecuritylevel($securitylevel):void
    {
        $this->securitylevel = $securitylevel;
    }

    /**
     * Returns the lastupdpwd
     *
     * @return string|null $lastupdpwd
     */
    public function getLastupdpwd(): ?string
    {
        return $this->lastupdpwd;
    }
    /**
     * Sets the lastupdpwd
     *
     * @param \DateTime $lastupdpwd
     * @return void
     */
    public function setLastupdpwd($lastupdpwd):void
    {
        $this->lastupdpwd = $lastupdpwd;
    }

    /**
     * Returns the lstinlog
     *
     * @return string|null $lstinlog
     */
    public function getlstinlog(): ?string
    {
        return $this->lstinlog;
    }
    /**
     * Sets the lstinlog
     *
     * @param string $lstinlog
     * @return void
     */
    public function setlstinlog($lstinlog):void
    {
        $this->lstinlog = $lstinlog;
    }

    /**
     * Returns the aantalmaalingelogd
     *
     * @return int $aantalmaalingelogd
     */
    public function getAantalmaalingelogd(): int
    {
        return $this->aantalmaalingelogd;
    }
    /**
     * Sets the aantalmaalingelogd
     *
     * @param int $aantalmaalingelogd
     * @return void
     */
    public function setaantAlmaalingelogd($aantalmaalingelogd):void
    {
        $this->aantalmaalingelogd = $aantalmaalingelogd;
    }

    /**
     * Returns the geblokkeerd
     *
     * @return int|null $geblokkeerd
     */
    public function getGeblokkeerd(): ?int
    {
        return $this->geblokkeerd;
    }
    /**
     * Sets the geblokkeerd
     *
     * @param int $geblokkeerd
     * @return void
     */
    public function setGeblokkeerd($geblokkeerd):void
    {
        $this->geblokkeerd = $geblokkeerd;
    }

    /**
     * Returns the txgoogleauthenabled
     *
     * @return int $txgoogleauthenabled
     */
    public function gettxgoogleauthenabled(): int
    {
        return $this->txgoogleauthenabled;
    }
    /**
     * Sets the txgoogleauthenabled
     *
     * @param int $txgoogleauthenabled
     * @return void
     */
    public function settxgoogleauthenabled($txgoogleauthenabled):void
    {
        $this->txgoogleauthenabled = $txgoogleauthenabled;
    }


}
