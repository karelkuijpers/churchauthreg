<?php
namespace Parousia\Churchauthreg\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/***
 *
 * This file is part of the "Churchperesreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * Search is a request to become part of the church
 */
class Search extends AbstractEntity
{
     /**
     * general
     *
     * @var string
     */
    protected $general = '';


/**
* Getters and Setters
*/

    /**
     * Returns the general
     *
     * @return string $general
     */
    public function getGeneral(): string
    {
        return $this->general;
    }
    /**
     * Sets the general
     *
     * @param string $general
     */
    public function setGeneral($general): void
    {
        $this->general = $general;
    }

}