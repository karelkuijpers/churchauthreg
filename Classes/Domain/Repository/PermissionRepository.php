<?php

namespace Parousia\Churchauthreg\Domain\Repository;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * Class PermissionRepository
 *
 * @package Parousia\Churchauthreg\Domain\Repository
 *
 * return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
 */
class PermissionRepository extends Repository
{
	var $ErrMsg=""; //error message
	var $admin=false;
	var $userid="";
	var $query;
	var $db;
	var $permission=array("bedieningstructuur lezen","bedieningstructuur schrijven","Cleanteams samenstellen","ContactArchief lezen","ContactArchief lezen vertrouwelijk","ContactArchief lezen verwijderd","ContactArchief schrijven","Email personen","Export adressenlijst","Export ledenlijst","Export personen","Gebouwbeheer","Netwerkgegevens lezen","Netwerkgegevens schrijven","Persoon lezen","Persoon schrijven","Persoon zoeken","Reserveringenbeheer","Smoelenboek Lezen","Verwijderde lezen");

	protected $aExport;

	
	public function findSelection(array $search = null)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);

		$statement=$this->GetPersons($search); 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelection('.$search['general'].'): '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Domain/Repository/debug.txt');
		$query->statement($statement);
		
 		try {
		      $result= $query->execute(true);
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		try {
		      return $result;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 
    }

	 /**
     * Override default findByUid function 
     *
     * param int  $uid                 id of record
     *
     * return persoon
     */
    public function findByUid($uid)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
        $statement=$this->getPerson($uid);
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findByUid statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');

//		var_dump($statement);
		$query->statement($statement);
        $results= $query->execute(true);
//		var_dump($results[0]);
		if(isset($results[0]))$persoon=$results[0];else $persoon=array();
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findByUid persoon: '.urldecode(http_build_query($persoon,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');

		return $persoon;
	}

	/**
	 * Method 'FindUsergroups' for the 'churchauthreg' extension.
 	*
	 * param void
	 * returns associative array with fields of the found usergroups 
	 */
	function FindUsergroups()
	{	
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		$statement = 'SELECT uid,title FROM  fe_groups as fg';
		$query->statement($statement);
        $usergroups= $query->execute(true);
/*		$usergroupoption=array();
		foreach ($usergroups as $usergroup)	$usergroupoption[$usergroup['uid']]=$usergroup['title'];
		return $usergroupoption; */
		return $usergroups;
	}

	/**
	 * Method 'FindAllUsergroups' for the 'churchauthreg' extension.
 	*
	 * param defaultusergroup
	 * returns associative array with fields of the found usergroups 
	 */
	function FindAllUsergroups($defaultusergroup)
	{	
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		$statement = 'SELECT gg.uid,gg.title,gg.description,'. 
		'group_concat(distinct concat(p.roepnaam," ",if(p.tussenvoegsel>"",concat(p.tussenvoegsel," "),""),p.achternaam) order by achternaam separator ",\n\r") as used, '.
		'group_concat(distinct concat(pg.uid," ",pg.title) order by pg.title separator ",\n\r") as pageaccess '.
		' FROM  fe_groups gg left join fe_users g on find_in_set(gg.uid,g.usergroup) left join persoon p on (g.person_id=p.uid  and gg.title<>"'.$defaultusergroup.'") '.
		'left join pages pg on (find_in_set(gg.uid,pg.fe_group) and pg.deleted=0) '.
		'group by gg.uid order by gg.title';

		$query->statement($statement);
        $usergroups= $query->execute(true);
		return $usergroups;
	}

	 /**
     * Override default findUsergroupByUid function 
     *
     * param int  $uid                 id of record
     *
     * return persoon
     */
    public function findUsergroupByUid($uid)
	{
		$usergroup=array();
		if ($uid)
		{
			$query = $this->createQuery();
	        $query->getQuerySettings()->setRespectStoragePage(FALSE);
    	    $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        	$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
	        $statement = 'SELECT gg.uid,gg.title,gg.description,f.function_uids, exists(select 1 from fe_users g where find_in_set(gg.uid,g.usergroup) limit 1) as used'.
			' FROM fe_groups as gg, (select group_concat(f_uid separator ",") as function_uids from rm_gg_f where gg_uid="'.$uid.'" ) as f where gg.uid="'.$uid.'"';
	
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findUsergroupByUid statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
	
	//		var_dump($statement);
			$query->statement($statement);
	        $results= $query->execute(true);
	//		var_dump($results[0]);
			if(isset($results[0]))$usergroup=$results[0];else $usergroup=array();
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findUsergroupByUid usergroup: '.urldecode(http_build_query($usergroup,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		}
		return $usergroup;
	}


	/**
	 * Method 'FindAllFunctionalities' for the 'churchauthreg' extension.
 	*
	 * returns associative array with fields of the found functionalities 
	 */
	function FindAllFunctionalities()
	{	
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		
		$statement='SELECT distinct f.f_uid,f.naam,f.omschrijving,f.securitylevel,group_concat(gg.title order by gg.title separator ",\n\r") as used '.
		' FROM rm_functionaliteit f left join rm_gg_f gg_f on (gg_f.f_uid=f.f_uid) left join fe_groups gg on (gg_f.gg_uid=gg.uid) group by f.f_uid order by f.naam';
		
	//	'SELECT distinct f.naam FROM rm_functionaliteit f, rm_f_fg f_fg, rm_fg_gg fg_gg WHERE f.f_uid=f_fg.f_uid AND f_fg.fg_uid = fg_gg.fg_uid AND '.
	//	'FIND_IN_SET(fg_gg.uid,(select usergroup from fe_users where uid="'.$feuserid.'")) order by f.naam;';

		$query->statement($statement);
        $functionalities= $query->execute(true);
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindFunctionalities statement:'.$statement.'; functionalities: '.urldecode(http_build_query($functionalities,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		return $functionalities;
	}

		/**
	 * Method 'InitializeAuth' for the 'churchauthreg' extension.
	 * 
	 * Initialize admin and default usergroup and functiononality
 	*
	 * returns void
	 */
	function InitializeAuth($pid,$defaultusergroup,$requiredpermissions)
	{	
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		$nofunctionality=0;
		// check default usergroup:
		$statement="SELECT fg.uid,fg.title FROM fe_groups fg where fg.title='".$defaultusergroup."'";
		$query->statement($statement);
        $usergroups= $query->execute(true);
		if (empty($usergroups))
		{
			$usergroup=['pid' => $pid, 'title' => $defaultusergroup, 'description' => "Default usergroup for everyone"];
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth save new usergroup: '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
			$this->saveUsergroup($usergroup);
		}

		// check admin functionality:
		$statement="SELECT f.* from rm_functionaliteit f where f.naam='".churchpersreg_div::ADMINFUNC."'";
		$query->statement($statement);
        $functionalities= $query->execute(true);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth start'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 

		if (empty($functionalities))
		{
			$functionality = [
				'naam' => churchpersreg_div::ADMINFUNC,
				'omschrijving' => "Management of permissions",
				'securitylevel' => "zwaar",
			];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth save new functionality: '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
			$this->saveFunctionality($functionality);
			$nofunctionality=1;
		}
		else
		{
			$functionality=$functionalities[0];
			if ($functionality['omschrijving']<>"Management of permissions" or $functionality['securitylevel']<>"zwaar")
			{
				$functionality['omschrijving']="Management of permissions";
				$functionality['securitylevel']="zwaar";
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth update functionality: '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
				$this->saveFunctionality($functionality);			
			}
		}

		// check administrator usergroup:
		$statement="SELECT fg.uid,fg.title,fg.description,pid, group_concat(gf.f_uid separator ',') as function_uids FROM fe_groups fg left join rm_gg_f as gf on (gf.gg_uid=fg.uid) where fg.title='".churchpersreg_div::ADMINGROUP."'";
		$query->statement($statement);
        $usergroups= $query->execute(true);
		if (empty($usergroups))
		{
			$usergroup=['pid' => $pid, 'title' => churchpersreg_div::ADMINGROUP, 'description' => "Usergroup for administrators with full authorisation",'function_uids' => array($functionality['f_uid'])];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth save new usergroup: '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
			$this->saveUsergroup($usergroup);
			$nousergroup=1;
		}
		else
		{
			$usergroup=$usergroups[0];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth no new usergroup functionality f_uid: '.$functionality['f_uid'].'; usergroup: '.urldecode(http_build_query($usergroup,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
			if ($usergroup['description']<>"Usergroup for administrators with full authorisation" or (string)$usergroup['function_uids']!== (string)$functionality['f_uid'] or $nofunctionality)
			{
				$usergroup['description']="Usergroup for administrators with full authorisation";
				$usergroup['function_uids']=array($functionality['f_uid']);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth update usergroup usergroup: '.urldecode(http_build_query($usergroup,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
				$this->saveUsergroup($usergroup);
			}
		}
		// check other functionalities used by church extensions:
		$statement="SELECT group_concat(naam separator ',') as names from rm_functionaliteit where naam<>'admin' order by naam";
		$query->statement($statement);
        $functionalities= $query->execute(true);
		$aNames=explode(',',$functionalities[0]['names']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth add permission found: '.urldecode(http_build_query($aNames,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 			
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth add required permission: '.urldecode(http_build_query($requiredpermissions,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 			
		$FunctionalityAdd=array_udiff($requiredpermissions,$aNames,'strcasecmp');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth difference permission: '.urldecode(http_build_query($FunctionalityAdd,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 			
		if (!empty($FunctionalityAdd))
		{
			foreach ($FunctionalityAdd as $permission)
			{
				$functionality = [
				'naam' => $permission,
				'omschrijving' => "",
				'securitylevel' => "licht",
				];
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitializeAuth add required permission: '.$functionality."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 			
				$this->saveFunctionality($functionality);
			}
		}
		
		

		
	}


	/**
	 * Method 'FindFunctionalities' for the 'churchauthreg' extension.
 	*
	 * param $feuserid
	 * returns associative array with fields of the found functionalities 
	 */
	function FindFunctionalities($feuserid = NULL)
	{	
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		
		$statement='SELECT distinct f.f_uid,f.naam,f.omschrijving,f.securitylevel FROM rm_functionaliteit f';
		if (!empty($feuserid))$statement.=', rm_gg_f as gg_f WHERE f.f_uid=gg_f.f_uid and FIND_IN_SET(gg_f.gg_uid,(select usergroup from fe_users where uid="'.$feuserid.'"))';
		$statement.=' order by f.naam';
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindFunctionalities statement:'.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		
	//	'SELECT distinct f.naam FROM rm_functionaliteit f, rm_f_fg f_fg, rm_fg_gg fg_gg WHERE f.f_uid=f_fg.f_uid AND f_fg.fg_uid = fg_gg.fg_uid AND '.
	//	'FIND_IN_SET(fg_gg.uid,(select usergroup from fe_users where uid="'.$feuserid.'")) order by f.naam;';

		$query->statement($statement);
        $functionalities= $query->execute(true);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindFunctionalities  functionalities: '.urldecode(http_build_query($functionalities,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		return $functionalities;
	}
	
	 /**
     * Override default findFunctionalityByUid function 
     *
     * param int  $uid                 id of record
     *
     * return persoon
     */
    public function findFunctionaltyByUid($uid)
	{
		$Functionality=array();
		if ($uid)
		{
			$query = $this->createQuery();
	        $query->getQuerySettings()->setRespectStoragePage(FALSE);
    	    $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        	$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
			$f_uid=intval($uid);
	        $statement = 'SELECT f.f_uid,f.naam,f.omschrijving,f.securitylevel, exists(select 1 from rm_gg_f gg_f where gg_f.f_uid='.$f_uid.' limit 1) as used FROM rm_functionaliteit f where f.f_uid='.$f_uid;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findFunctionalityByUid statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
	
	//		var_dump($statement);
			$query->statement($statement);
	        $results= $query->execute(true);
	//		var_dump($results[0]);
			$Functionality=$results[0];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findFunctionalityByUid Functionality: '.urldecode(http_build_query($Functionality,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		}
		else
		{
			$Functionality['f_uid']=0;
			$Functionality['naam']='';
			$Functionality['omschrijving']='';
			$Functionality['securitylevel']='middel';
			$Functionality['used']=0;
		}
		return $Functionality;
	}


	
	/**
	 * Method 'FindOverviewFunctionalities' for the 'churchauthreg' extension.
 	*
	 * returns associative array with fields of the found functionalities and users 
	 */
	function FindOverviewFunctionalities($defaultusergroup)
	{	
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		$statement= 'select distinct f.naam as Permissie,'.
		'group_concat(distinct concat(p.roepnaam," ",if(p.tussenvoegsel>"",concat(p.tussenvoegsel," "),""),p.achternaam) order by achternaam separator "\r\n") as Naam '.
		'from rm_functionaliteit as f left join rm_gg_f as gg_f on(f.f_uid=gg_f.f_uid)  '.
		'left join fe_groups gg on (gg_f.gg_uid=gg.uid) '.
		'left join fe_users u on FIND_IN_SET(gg_f.gg_uid,u.usergroup) left join persoon p on (u.person_id=p.uid) '.
		'where gg.title<>"'.$defaultusergroup.'" group by f.naam order by f.naam;';
//		'where f.securitylevel>"licht" and gg_f.gg_uid<>8 group by f.naam order by f.naam;';

		$query->statement($statement);
        $functionalities= $query->execute(true);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindFunctionalities statement:'.$statement.'; functionalities: '.GeneralUtility::array2xml($functionalities)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');
		return $functionalities;
	}

	/**
	 * Method 'SaveUser' for the 'churchauthreg' extension.
 	*
	 * param person required 
	 * returns true or error message
	 */
	
	public function saveUser(&$permission)
	{
		$this->ErrMsg="";
		if (! churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 

		$feuser_uid=intval($permission['feuser_uid']);

		// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
		$statement='update fe_users set usergroup="'.implode(",",$permission['usergroup']).'",geblokkeerd='.intval($permission['geblokkeerd']).',tx_googleauth_enabled='.intval($permission['tx_googleauth_enabled']).',LoginErrorCount=0 where uid='.$feuser_uid;
		$result=$this->db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveUser statement: '.$statement."; error:".$this->db->error.", affected:".$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		return $this->ErrMsg;
	}
	
	/**
	 * Method 'saveUsergroup' for the 'churchauthreg' extension.
 	*
	 */
	
	public function saveUsergroup($usergroup)
	{
		$this->ErrMsg="";
		if (! churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 

		$usergroup_uid=intval($usergroup['uid']);
		$insert=empty($usergroup_uid);

		// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
		if (empty($usergroup_uid))$statement='insert into fe_groups set pid="'.intval($usergroup['pid']).'",title="'.$this->db->escape_string($usergroup['title']).'",description="'.$this->db->escape_string($usergroup['description']).'" ';
		else $statement='update fe_groups set title="'.$this->db->escape_string($usergroup['title']).'",description="'.$this->db->escape_string($usergroup['description']).'" where uid='.$usergroup_uid;
		//usergroup="'.implode(",",$permission['usergroup']).'",geblokkeerd='.intval($permission['geblokkeerd']).' where uid='.$feuser_uid;
		$result=$this->db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveUsergroup statement: '.$statement."; error:".$this->db->error.", affected:".$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		if ($insert)
		{
			$usergroup_uid=$this->db->insert_id;
			$usergroup['uid']=$usergroup_uid;
		}
		if (!empty($usergroup["function_uids"]))
		{
			// save new functionalities:
			$statement='insert ignore into rm_gg_f (gg_uid,f_uid) value ';
			$function_uids=implode(",",$usergroup["function_uids"]);
			foreach ($usergroup["function_uids"] as $function) $statement.='('.$usergroup_uid.','.$function.'),';
			$statement=substr($statement,0,-1);
			$result=$this->db->query($statement);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveUsergroup rm_gg_g statement: '.$statement.'; function_uids: '.$function_uids."; error:".$this->db->error.", affected:".$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
			if (!$insert)
			{
				// remove omitted functionalities
				$statement="delete from rm_gg_f where gg_uid=".$usergroup_uid." and not FIND_IN_SET(f_uid,'".$function_uids."')";
				$result=$this->db->query($statement);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveUsergroup delete rm_gg_g statement: '.$statement."; error:".$this->db->error.", affected:".$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 	
			}
		}
		elseif (!$insert)
		{
				// remove omitted functionalities
				$statement="delete from rm_gg_f where gg_uid=".$usergroup_uid;
				$result=$this->db->query($statement);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveUsergroup delete all rm_gg_g statement: '.$statement."; error:".$this->db->error.", affected:".$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 	
		}
		return $this->ErrMsg;
	}
	
	 /**
     * DeleteUsergroup function 
     *
     * param int  $uid                 id of usergroup
     *
     * return void
     */
    public function deleteUsergroup($uid)
	{
		// initialize mysqli:
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete person start personid:'.$personid.'; id_adres:'.$id_adres."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		churchpersreg_div::connectdb($this->db);
		$usergroupid=intval($uid);
		$statement="delete from fe_groups  where uid=".$usergroupid;
		$results=$this->db->query($statement);
		
		// delete corresponing relation records to functions rm_gg_f
		$statement="delete from rm_gg_f where gg_uid=".$usergroupid;
		$results=$this->db->query($statement);
		
	//	$ErrMsg=$this->db->error;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete bedieningsprofiel: '.$statement."; error:".$ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		// verwijder ook bijbehorende contactinfo:
		
		return '';
	}

		
	/**
	 * Method 'saveFunctionality' for the 'churchauthreg' extension.
 	*
	 */
	
	public function saveFunctionality($functionality)
	{
		$this->ErrMsg="";
		if (! churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		if (empty($functionality)) return '';
		if(isset($functionality['f_uid']))$functionality_uid=intval($functionality['f_uid']);else $functionality_uid=null;
		$insert=empty($functionality_uid);
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveFunctionality functionalitiy: '.urldecode(http_build_query($functionality,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt');

		// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
		if ($insert)$statement='insert into'; else $statement='update';
		$statement.=' rm_functionaliteit set naam="'.$this->db->escape_string($functionality['naam']).'",omschrijving="'.$this->db->escape_string($functionality['omschrijving']).'",securitylevel="'.$this->db->escape_string($functionality['securitylevel']).'"';
		if (!$insert) $statement.=' where f_uid='.$functionality_uid; else $statement.=' ON DUPLICATE KEY UPDATE naam=naam';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveFunctionality statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		//functionality="'.implode(",",$permission['functionality']).'",geblokkeerd='.intval($permission['geblokkeerd']).' where uid='.$feuser_uid;
		$result=$this->db->query($statement);
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveFunctionality statement: '.$statement."; error:".$this->db->error.", affected:".$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		if ($insert)
		{
			$functionality_uid=$this->db->insert_id;
			$functionality['f_uid']=$functionality_uid;
		}
		return $this->ErrMsg;
	}
	
	 /**
     * DeleteFunctionality function 
     *
     * param int  $uid                 id of functionality
     *
     * return void
     */
    public function deleteFunctionality($uid)
	{
		// initialize mysqli:
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete person start personid:'.$personid.'; id_adres:'.$id_adres."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		churchpersreg_div::connectdb($this->db);
		$functionalityid=intval($uid);
		$statement="delete from rm_functionaliteit  where f_uid=".$functionalityid;
		$results=$this->db->query($statement);
		
	
	//	$ErrMsg=$this->db->error;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete bedieningsprofiel: '.$statement."; error:".$ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Controller/debug.txt'); 
		// verwijder ook bijbehorende contactinfo:
		
		return '';
	}


	
	/**
	 * Method 'GetPersons' for the 'churchauthreg' extension.
 	*
	 * param searchfield: keyword required persons
	 * returns string with select statement 
	 */
	function GetPersons(array $search = null)
	{	
	//	Samenstellen van zoekargument
	//	$this->ErrMsg="Getpersons zoekveld 1:".$Zoekveld_1.",2:".$Zoekveld_2.",3:".$Zoekveld_3.",4:".$Zoekveld_4.",5:".$Zoekveld_5.",6:".$Zoekveld_6;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetPersons:bezoeker: '.$Bezoeker."\r\n",3,'churchauthreg_pi3.txt');

		$zoekargument=$this->ComposeSearch($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetPersons:bezoeker: '.$Bezoeker."; zoekargument:".$zoekargument."\r\n",3,'churchauthreg_pi3.txt');

		/* Performing SQL query */ 
		$statement = 'SELECT tp.uid as personuid, fe.uid as feuseruid'.
		', concat(tp.achternaam,", ",tp.roepnaam," ",tp.tussenvoegsel) as naam, tp.achternaam'.
		', straatnaam,huisnummer,postcode,woonplaats,land'.
		', fe.username,fe.securitylevel,fe.geblokkeerd,fe.tx_googleauth_enabled as txgoogleauthenabled'. 
		' FROM persoon as tp,adres as ta, fe_users as fe'.
		' where '.$zoekargument.
		' ORDER BY tp.achternaam'; 
		return ($statement);
	}
	
		/**
	 * Method 'ComposeSearch' for the 'churchauthreg' extension.
 	*
	 * param searchfield: keyword required persons ({$Zoekveld,$Zoekveld_1,$Zoekveld_2,$Zoekveld_3,$Zoekveld_4,$Zoekveld_5,$Zoekveld_6})
	 * returns composed sql search parameter
	 */
	function ComposeSearch(array $search = null)
	{	
	//	Samenstellen van zoekargument
	//			$this->ErrMsg.="Compose zoekveld 1:".$Zoekveld[1].",2:".$Zoekveld[2].",3:".$Zoekveld[3].",4:".$Zoekveld[4].",5:".$Zoekveld[5].",6:".$Zoekveld[6].",7:".$Zoekveld[7];
		$zoekargument="";
		$Not=false;
		$connection=\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getConnectionForTable('string');
	/*	if (!empty($Zoekveld[7])){
			$zoekargument .=  "(tp.uid = '" . addslashes($Zoekveld[7]) ."') AND ";}		*/
		if ($search['general']<>""){ 
			$a_key=$this->DeriveKeywords($search['general']);
				//	TYPO3\CMS\Core\Utility\GeneralUtility::devLog("zoekveld:".$Zoekveld[0],"churchauthreg");
			foreach ($a_key as $key)
			{	$key=trim($key);
				if (!empty($key))
				{	// key:
					if (substr($key,0,1)=="-")
					{	
						$zoekargument.="NOT ";  // ontkenning
						$key=substr($key,1);
						$Not=true;
					}			
					$key=addslashes($key);
						//TYPO3\CMS\Core\Utility\GeneralUtility::devLog("zoekwoord:".$key,"churchauthreg");
					$zoekargument.=' (concat(tp.roepnaam," ",tp.tussenvoegsel," ",tp.achternaam) like "%'.$key.'%" OR ';
					// ook op andere velden zoeken:
					$zoekargument .=  "straatnaam LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "huisnummer LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "postcode LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "woonplaats LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "tp.geslacht LIKE '" . $key ."%' OR ";
					$zoekargument .=  "tp.burgerlijke_staat LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "tp.gedoopt LIKE '" . $key ."%' OR ";
					$zoekargument .=  "tp.lid LIKE '" . $key ."%' OR ";
					$zoekargument .=  "fe.securitylevel LIKE '" . $key ."%' OR ";
					if ($key=='geblokkeerd')	$zoekargument .=  "fe.geblokkeerd = 1 OR ";
					if ($key=='2FA')	$zoekargument .=  "fe.tx_googleauth_enabled = 1 OR ";
					$zoekargument .=  "fe.username LIKE '%" . $key ."%') and ";	
				}
			}
		}
		$zoekargument.=' AES_DECRYPT(tp.id_adres,@password)=ta.uid  and fe.person_id=tp.uid and tp.deleted=0 and ta.deleted=0';
//		$this->ErrMsg.=",br>Zoekargument:".$zoekargument;
		return $zoekargument;
	}
	
	/*
	 * function DeriveKeywords:
	 * Derive search keywords from a Google-like searchfield $strZoek
	 */
	function DeriveKeywords($strZoek){
		// bepalen van zoekwoorden uit een vrije tekst veld
		$aZoekwoord=array();
		$aZoekveld=array();
		$Quote="";
		$WordQ="";
		$Min="";
		// Replace &quote; back to ":
		$strZoek=str_ireplace("&quot;", '"', $strZoek);
		// splits in losse woorden:
		$aZoekwoord = preg_split ("/[\s,;]+/", trim($strZoek)); 
		// voeg strings tussen quote's weer samen:
		for ($i=0;$i<count($aZoekwoord);++$i)
		{	$Woord=$aZoekwoord[$i];
			if (empty($Quote))
			{	// zoeken naar quote:
				// eerst checken op +-
				if (substr($Woord,0,1)=="-")
				{	$Min="-";
					$Woord=substr($Woord,1);
				}
				else
				{	$Min="";
					if (substr($Woord,0,1)=="+"){$Woord=substr($Woord,1);} //+ schrappen
				}
					
				if (substr($Woord,0,1)=='"' or substr($Woord,0,1)=="'")
				{	$Quote=substr($Woord,0,1);
					$aZoekwoord[$i]=$Min.substr($Woord,1);
					$Min="";
					--$i; // zoekwoord nogmaals meenemen voor verwerking
				}
				else
				{	array_push($aZoekveld,addslashes($Min.$Woord));
				}
			}
			else
			{	// zoeken naar eind quote:
				if (substr($Woord,strlen($Woord)-1,1)==$Quote)
				{	$WordQ.=" ".substr($Woord,0,strlen($Woord)-1);
					array_push($aZoekveld,trim(addslashes($WordQ)));
					$WordQ="";
					$Quote="";
				}
				else
				{	$WordQ.=" ".$Woord;
				}
			}
		}
		if (!empty($WordQ)){array_push($aZoekveld,$WordQ);} // laatste tot eind ook meenemen
		return $aZoekveld;
	}
	
	/**
	 * Method 'GetPerson' for the 'churchauthreg' extension.
 	*
	 * param person_id: uid of selected person
	 * returns associative array with fields of the found person 
	 */
	function GetPerson($person_id)
	{	
		$statement = 'SELECT DISTINCT tp.uid as personuid'.
		', concat(tp.roepnaam," ",tp.tussenvoegsel," ",tp.achternaam) as naam'.
		', concat(straatnaam," ",huisnummer," ",postcode," ",woonplaats) as adres'.
		', fe.uid as feuseruid,fe.username,fe.pid,fe.usergroup,fe.geblokkeerd,fe.tx_googleauth_enabled as txgoogleauthenabled,cast(fe.lastUpdPwd as DATETIME) as lastupdpwd,fe.securitylevel,fe.LstInlog as lstinlog,fe.AantalMaalIngelogd as aantalmaalingelogd'.
		' FROM  adres as ap, persoon as tp, fe_users as fe'.
		' where tp.uid="'.$person_id.'" and fe.person_id=tp.uid and AES_DECRYPT(tp.id_adres,@password)=ap.uid ';
		return $statement;
	}

	
	public function findExportPersons(array $search = null)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);

		$statement=$this->GetExportPersons($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findExportPersons: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchauthreg/Classes/Domain/Repository/debug.txt');
		$query->statement($statement);
		
 		try {
		      $result= $query->execute(true);
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		try {
		      return $result;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 
    }


	function cmp ($a, $b) { 
	   return strcmp($a[0], $b[0]); 
	} 

	/**
	 * Method 'GetExportPersons' for the 'churchauthreg' extension.
 	*
	 * param searchfield: keyword required persons
	 * returns string with select statement 
	 */
	function GetExportPersons(array $search = null)
	{	
	//	Samenstellen van zoekargument

		$zoekargument=$this->ComposeSearch($search);
		
		$statement = 'SELECT tp.uid as person_uid, fe.uid as feuser_uid'.
		', concat(tp.achternaam,", ",tp.roepnaam," ",tp.tussenvoegsel) as naam, tp.achternaam, tp.roepnaam, tp.geslacht'.
		', AES_DECRYPT(tp.emailadres,@password) as emailadres'.
		', AES_DECRYPT(tp.mobieltelnr,@password) as mobieltelnr'.
		', straatnaam,huisnummer,postcode,woonplaats,land'.
		', fe.username,fe.securitylevel'. 
		' FROM persoon as tp,adres as ta, fe_users as fe'.
		' where '.$zoekargument.
		' ORDER BY tp.achternaam'; 

		return ($statement);
	}

	
}