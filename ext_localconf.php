<?php
defined('TYPO3') || die('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
         'Churchauthreg',
         'Permission',
         [
             \Parousia\Churchauthreg\Controller\PermissionController::Class => 'searchUser,detailUser,selectedList,saveUser,detailUsergroup,selectedListUsergroup,saveUsergroup,deleteUsergroup,detailFunctionality,selectedListFunction,saveFunctionality,deleteFunctionality,overview,export,',
         ],
         // non-cacheable actions
		 [
             \Parousia\Churchauthreg\Controller\PermissionController::Class => 'searchUser,detailUser,selectedList,saveUser,detailUsergroup,selectedListUsergroup,saveUsergroup,deleteUsergroup,detailFunctionality,selectedListFunction,saveFunctionality,deleteFunctionality,overview,export,',
         ]
     );
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['usergrouppermissions'] = \Parousia\Churchauthreg\Hooks\usergrouppermissions::class .'::processRequest';


