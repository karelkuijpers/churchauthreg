
#
# Table structure for table `rm_functionaliteit`
#

CREATE TABLE `rm_functionaliteit` (
  `f_uid` int(11) NOT NULL,
  `naam` varchar(50) DEFAULT NULL,
  `omschrijving` varchar(255) DEFAULT NULL,
  `securitylevel` enum('licht','middel','zwaar') NOT NULL DEFAULT 'zwaar',
   PRIMARY KEY (`f_uid`),
   UNIQUE KEY `functionname` (`naam`),
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;

#
# Table structure for table `fe_groups`
#

CREATE TABLE `fe_groups` (
   UNIQUE KEY `grouptitle` (`title`),
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;

#
# Table structure for table 'rm_gg_f'
#
CREATE TABLE `rm_gg_f` (
  `gg_uid` int(11) NOT NULL,
  `f_uid` int(11) NOT NULL,
  PRIMARY KEY (`gg_uid`,`f_uid`),
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8;

#
# Table structure for table 'fe_users'
#
CREATE TABLE `fe_users` (
  `geblokkeerd` tinyint(1) DEFAULT NULL,
  `lastUpdPwd` varchar(14) DEFAULT NULL,
  `updFreqency` int(6) DEFAULT NULL,
  `pswPrevious1` blob,
  `person_id` int(11) DEFAULT NULL,
  `securitylevel` enum('licht','middel','zwaar') NOT NULL DEFAULT 'zwaar',
  `huidiginloglevel` enum('licht','middel','zwaar') NOT NULL DEFAULT 'licht',
  `LstInlog` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `AantalMaalIngelogd` int(11) NOT NULL DEFAULT '0',
)  ENGINE=InnoDB DEFAULT CHARACTER SET utf8;

